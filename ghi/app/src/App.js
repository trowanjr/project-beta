import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import TechnicianForm from './TechnicianForm';
import AppointmentForm from './AppointmentForm';
import AppointmentList from './AppointmentList';
import ServiceHistoryForm from './ServiceHistoryForm';
import CustomerList from './components/Customer/CustomerList';
import CustomerForm from './components/Customer/CustomerForm';
import SalesList from './components/Sales/SalesList';
import SalesRecordForm from './components/Sales/SalesRecordForm';
import SalesMemberList from './components/Sales/SalesMemberList';
import SalesMemberForm from './components/Sales/SalesMemberForm';
import SalesMembersSales from './components/Sales/SalesMembersSales';
import ManufacturerList from './components/Manufacturer/ManufacturerList';
import ManufacturerForm from './components/Manufacturer/ManufacturerForm';
import ModelList from './components/Model/ModelList';
import ModelForm from './components/Model/ModelForm';
import VehicleList from './components/Vehicle/VehicleList';
import VehicleForm from './components/Vehicle/VehicleForm';
import Footer from './components/Footer.js'


function App() {
  return (
    <>
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path='/technicianform' element={<TechnicianForm />} />
          <Route path='/appointmentform' element={<AppointmentForm />} />
          <Route path='appointmentlist' element={<AppointmentList />} />
          <Route path='servicehistoryform' element={<ServiceHistoryForm />} />
          <Route path='servicehistoryform' element={<ServiceHistoryForm />} />
          <Route path="/customers/" element={<CustomerList />} />
          <Route path="/customer/new" element={<CustomerForm/>} />
          <Route path="/sales" element={<SalesList/>} />
          <Route path="/sales/members" element={<SalesMembersSales/>} />
          <Route path="/sales/members/new" element={<SalesMemberForm/>} />
          <Route path="/sales/members/list" element={<SalesMemberList/>} />
          <Route path="/sales/records" element={<SalesRecordForm/>} />
          <Route path="/manufacturers" element={<ManufacturerList/>} />
          <Route path="/manufacturers/new" element={<ManufacturerForm/>} />
          <Route path="/models" element={<ModelList/>} />
          <Route path="/models/new" element={<ModelForm/>} />
          <Route path="/vehicles" element={<VehicleList/>} />
          <Route path="/vehicles/new" element={<VehicleForm/>} />
        </Routes>
      </div>
    </BrowserRouter>
    <Footer />
    </>
  );
}

export default App;
