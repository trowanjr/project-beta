function Nav() {
    return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
        <div className="container-fluid">
            <NavLink className="navbar-brand" to="/">CarCar</NavLink>
            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                    <li className='nav-item'>
                    <NavLink className='nav-link active' aria-current='page' to='/'>Home</NavLink>
                    </li>
                    <li className='nav-item'>
                        <div className="nav-link active dropdown-toggle" data-toggle="dropdown">Service Management</div>
                        <div className="dropdown-menu">
                        <NavLink className='dropdown-item' to='/technicianform'>Technician Form</NavLink>
                        <NavLink className='dropdown-item' to='/appointmentform'>Appointment Form</NavLink>
                        <NavLink className='dropdown-item' to='/appointmentlist'>Appointment List</NavLink>
                        <NavLink className='dropdown-item' to='/servicehistoryform'>Service History Form</NavLink>
                        </div>
                    </li>
                    <li className='nav-item'>
                        <div className="nav-link active dropdown-toggle" data-toggle="dropdown">Customer and Sales</div>
                        <div className="dropdown-menu">
                        <NavLink className="dropdown-item" to="/customer/new">Add Customer</NavLink>
                        <NavLink className="dropdown-item" to="/customers">Customer List</NavLink>
                        <NavLink className="dropdown-item" to="/sales/members/new">Add Sales Member</NavLink>
                        <NavLink className="dropdown-item" to="/sales/members/list">View Sales Team</NavLink>
                        <NavLink className="dropdown-item" to="/sales/members">Sales Team Accomplishments</NavLink>
                        <NavLink className="dropdown-item" to="/sales/records">Add Sales Record</NavLink>
                        <NavLink className="dropdown-item" to="/sales">Sales List</NavLink>
                        </div>
                    </li>
                    <li className='nav-item'>
                        <div className="nav-link active dropdown-toggle" data-toggle="dropdown">Vehicle Management</div>
                        <div className="dropdown-menu">
                        <NavLink className="dropdown-item" to="/manufacturers">Manufacturers</NavLink>
                        <NavLink className='dropdown-item' to="manufacturers/new">Create Manufacturer</NavLink>
                        <NavLink className='dropdown-item' to="/models">Models</NavLink>
                        <NavLink className='dropdown-item' to="models/new">Create Model</NavLink>
                        <NavLink className='dropdown-item' to="/vehicles">Vehicles</NavLink>
                        <NavLink className='dropdown-item' to="vehicles/new">Create Vehicle</NavLink>
                    </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    )
}

export default Nav;