import { useState, useEffect } from "react";

function TechnicianForm() {
    const [formData, setFormData] = useState({
        name: "",
        employee_num: '',
    })

    const handleSubmit = async (event) => {
        event.preventDefault();

        const url = 'http://localhost:8080/api/technician/';

        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            setFormData({
                name: '',
                employee_num: '',
            });
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value
        });

    }
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a Technician</h1>
                    <form onSubmit={handleSubmit} id="create-tech-form">
                        <div className="form-floating-mb-3">
                            <input onChange={handleFormChange} value={formData.name} placeholder="Name" required type="text" name="name" id="name" className="form-content" />
                            <label htmlFor="name"></label>
                        </div>
                        <div className="form-floating-mb-3">
                            <input onChange={handleFormChange} value={formData.employee_num} placeholder="Employee Number" required type="text" name="employee_num" id="employee_num" className="form-content" />
                            <label htmlFor="employee_num"></label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default TechnicianForm;
