import React, { useState, useEffect } from 'react'


function AppointmentForm() {
    const [technicians, setTechnicians] = useState([])


    const getData = async () => {
        const url = 'http://localhost:8080/api/technician/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.techs);
        }
    }

    useEffect( () => {
        getData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}
        data.vin = vin
        data.cust_name = cust_name
        data.date = date
        data.time = time
        data.reason = reason
        data.technician = technician


        const url= 'http://localhost:8080/api/appt/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const newAppointment = await response.json();


            setVin('')
            setCustName('')
            setDate('')
            setTime('')
            setReason('')
            setTechnician('')
        }
    }


    const [vin, setVin] = useState('')
    const handleVinChange = (e) => {
        const value = e.target.value
        setVin(value)
    }

    const [cust_name, setCustName] = useState('')
    const handleCustNameChange = (e) => {
        const value = e.target.value
        setCustName(value)
    }

    const [date, setDate] = useState('')
    const handleDateChange = (e) => {
        const value = e.target.value
        setDate(value)
    }

    const [time, setTime] = useState('')
    const handleTimeChange = (e) => {
        const value = e.target.value
        setTime(value)
    }

    const [reason, setReason] = useState('')
    const handleReasonChange = (e) => {
        const value = e.target.value
        setReason(value)
    }

    const [technician, setTechnician] = useState('')
    const handleTechnicianChange = (e) => {
        const value = e.target.value
        setTechnician(value)
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                <h1>Create An Appointment</h1>
                <form onSubmit={handleSubmit} id="create-appointment-form">
                    <div className="form-floating mb-3">
                    <input onChange={handleVinChange} value={vin} placeholder="Vin" required type="text" name="vin" id="vin" className="form-control" />
                    <label htmlFor="vin">Vin</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input onChange={handleCustNameChange} value={cust_name} placeholder="customer name" required type="text" name="cust_name" id="cust_name" className="form-control" />
                    <label htmlFor="cust_name">Customer Name</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input onChange={handleDateChange} value={date} placeholder="date" required type="text" name="date" id="data" className="form-control" />
                    <label htmlFor="date">Date</label>
                    </div>
                    <div></div>
                    <div className="form-floating mb-3">
                    <input onChange={handleTimeChange} value={time} placeholder="Time" required type="text" name="time" id="time" className="form-control" />
                    <label htmlFor="time">Time</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input onChange={handleReasonChange} value={reason} placeholder="Reason" required type="text" name="reason" id="reason" className="form-control" />
                    <label htmlFor="reason">Reason</label>
                    </div>
                    <div className="mb-3">
                    <select onChange={handleTechnicianChange} value={technician} required name="technician" className="form-select" >
                        <option value="">Choose Technician </option>
                        {technicians.map(technician => {
                            return (
                                <option key={technician.id} value={technician.employee_num}>{technician.name}</option>
                            );
                        })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
                </div>
            </div>
        </div>
  );
}

export default AppointmentForm;
