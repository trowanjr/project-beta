import { useState, useEffect } from "react";

function ModelForm() {
    const [manufacturers, setManufacturers] = useState([])

    const getData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/';
        const res = await fetch(url);

        if (res.ok) {
            const data = await res.json();
            setManufacturers(data.manufacturers)
        }
    }

    useEffect(() => {
        getData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {}
        data.name = name
        data.picture_url = picture_url
        data.manufacturer_id = manufacturer

        const url= 'http://localhost:8100/api/models/'
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const res = await fetch(url, fetchConfig);
        if (res.ok) {
            const newModel = await res.json();
            setHasSignedUp(true)
            newModel(data)
            setName('')
            setPicture('')
            setManufacturers('')
        }
    }

    const [name, setName] = useState('')
    const handleNameChange = (e) => {
        const value = e.target.value
        setName(value)
    }

    const [picture_url, setPicture] = useState('')
    const handlePictureChange = (e) => {
        const value = e.target.value
        setPicture(value)
    }

    const [manufacturer, setManufacturer] = useState('')
    const handleManufacturerChange = (e) => {
        const value = e.target.value
        setManufacturer(value)
    }

    const [hasSignedUp, setHasSignedUp] = useState(false)
    const formClasses = (!hasSignedUp) ? '' : 'd-none';
    const messageClasses = (!hasSignedUp) ? 'alert alert-success d-none mb-0' : 'alert alert-success mb-0';

    const [hasFailed, setHasFailed] = useState(false)
    const formClass = (!hasFailed) ? '' : 'd-none';
    const messageClass=(!hasFailed) ? 'alert alert-warning d-none mb-0' : 'alert alert-danger mb-0';

    return (
        <div className="container shadow">
            <div className="card shadow p-3">
                <form className={formClasses - formClass} onSubmit={handleSubmit}>
                    <h1 className="text-center">New Model</h1>
                    <div className="form-floating row">
                        <input onChange={handleNameChange} className="form-control" required name="name"></input>
                        <label>Name</label>
                    </div>
                    <div className="form-floating row">
                        <input onChange={handlePictureChange} className="form-control" required name="picture_url"></input>
                        <label>Picture URL</label>
                    </div>
                    <div className='form-floating row'>
                        <select onChange={handleManufacturerChange} value={manufacturer} required name='manufacturer_id' className='form-select'>
                            <option value=''>Choose Manufacturer</option>
                            {manufacturers.map(manufacturer => {
                                return (
                                    <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                                )
                            })}
                        </select>
                    </div>
                    <div className="p-2 row">
                        <button className="btn btn-primary">Save</button>
                    </div>
                </form>
                <div className={messageClasses}  id="success-message">
                    <p className="text-center">New Model Added!</p>
                </div>
                <div className={messageClass} id="warning-message">
                    <p className="text-center">Your URL Length Is Too Long!!! Try Again</p>
                </div>
            </div>
        </div>
    )
}

export default ModelForm;
