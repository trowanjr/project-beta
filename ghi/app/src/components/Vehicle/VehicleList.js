import { useState, useEffect } from 'react';

function VehicleList() {
    const [automobiles, setAutomobiles] = useState([]);

    useEffect(() => {
        async function fetchData() {
            const response = await fetch('http://localhost:8100/api/automobiles/');
            const data = await response.json();
            setAutomobiles(data.autos)
        }
        fetchData();
    }, []);

    const handleDelete = async (id) => {
        const response = await fetch(`http://localhost:8100/api/automobiles/${id}`, {
            method: "DELETE",
        })
        if (response.ok) {
            setAutomobiles(automobiles.filter((automobile) => automobile.id !== id))
        }
    }

    return (
        <div>
            <h1 className="text-center">Automobile Inventory</h1>
            <table className='table table-striped'>
                <thead>
                    <tr>
                        <th>Vin</th>
                        <th>Color</th>
                        <th>Year</th>
                        <th>Model</th>
                        <th>Manufacturer</th>
                    </tr>
                </thead>
                <tbody>
                    {automobiles.map(automobile => (
                        <tr key={automobile.id}>
                            <td>{automobile.vin}</td>
                            <td>{automobile.color}</td>
                            <td>{automobile.year}</td>
                            <td>{automobile.model.name}</td>
                            <td>{automobile.model.manufacturer.name}</td>
                            <td>
                                <button onClick={() => handleDelete(automobile.id)}>Delete</button>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    )
}

export default VehicleList;
