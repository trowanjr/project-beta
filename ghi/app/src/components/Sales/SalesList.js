import React, { useState, useEffect } from "react";

function SalesList() {
  const [sales, setSales] = useState([]);

  const getData = async () => {
    const response = await fetch("http://localhost:8090/api/sales/");
    if (response.ok) {
      const data = await response.json();
      setSales(data.sales_record);
    }
  };

  const handleDelete = async (id) => {
    const response = await fetch(`http://localhost:8090/api/sales/${id}`, {
      method: "DELETE",
    });
    if (response.ok) {
      setSales(sales.filter((sale) => sale.id !== id));
    }
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <div>
      <h1 className="text-center">Sales History</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Sales Member</th>
            <th>Employee ID</th>
            <th>Customer Name</th>
            <th>Automobile VIN</th>
            <th>Sale's Price</th>
          </tr>
        </thead>
        <tbody>
          {sales.map((sale) => {
            return (
              <tr key={sale.id}>
                <td>{sale.sales_person.name}</td>
                <td>{sale.sales_person.employee_id}</td>
                <td>{sale.customer.name}</td>
                <td>{sale.automobile.vin}</td>
                <td>{sale.price}</td>
                <td>
                  <button onClick={() => handleDelete(sale.id)}>Delete</button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default SalesList;
