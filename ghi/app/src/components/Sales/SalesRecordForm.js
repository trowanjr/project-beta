import React, {useEffect, useState} from 'react';

function SaleRecordForm(){
    const [automobile, setAutomobile] = useState([])
    const [employee, setEmployee] = useState([])
    const [customers, setCustomers] = useState([])
    const [formData, setFormData] = useState({
    })

    const handleFormChange = e => {
        setFormData({
            ...formData,
            [e.target.name]:e.target.value
        })
    }

    const getData = async () => {
        const autoUrl = 'http://localhost:8100/api/automobiles/'
        const response = await fetch(autoUrl)
        if (response.ok) {
            const autoData = await response.json()
            setAutomobile(autoData.autos)}

        const employeeUrl = 'http://localhost:8090/api/employees/'
        const employeeResponse = await fetch(employeeUrl)
        if (employeeResponse.ok) {
            const employeeData = await employeeResponse.json()
            setEmployee(employeeData.sales_person)}

        const customerUrl = 'http://localhost:8090/api/customers/'
        const customerResponse = await fetch(customerUrl)
        if (customerResponse.ok) {
            const customerData = await customerResponse.json()
            setCustomers(customerData.customers)}
        }

    useEffect( ()=> {
        getData()
    }, [])

    const handleSubmit  = async event  => {
        event.preventDefault();
        const url = 'http://localhost:8090/api/sales/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {'Content-type':'application/json'}
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            const newResponse = await response.json()
            setHasSignedUp(true)
        }
    }

    const[sales, setSales] = useState([])
    const getSalesData = async () => {
        const response = await fetch("http://localhost:8090/api/sales/")
        if (response.ok) {
            const data = await response.json()
            setSales(data.sales_record)
        }
    }

    useEffect( () => {
        getSalesData()
    }, [])

    const soldVins = []
    for (let sale of sales) {
        soldVins.push(sale.automobile.vin)
    }

    const currentLot = []
    for (let auto of automobile) {
        if (!(soldVins.includes(auto.vin))) {
        currentLot.push(auto)
        }
    }

    const [hasSignedUp, setHasSignedUp] = useState(false)
    const formClasses = (!hasSignedUp) ? '' : 'd-none';
    const messageClasses = (!hasSignedUp) ? 'alert alert-success d-none mb-0' : 'alert alert-success mb-0';

    return (
        <div className="row">
            <div className="offset-2 col-5">
                <div className="shadow p-4 mt-5">
                    <h1 className="text-center">Create a sales record</h1>
                    <form className={formClasses} onSubmit={handleSubmit} id="create-salesRecord-form">
                    <div className="mb-3">
                    <select onChange={handleFormChange} required name="automobile" className="form-select" id="automobile">
                        <option>Choose a vehicle</option>
                        {currentLot.map(auto => {
                        return (
                        <option key={auto.id} value={auto.vin}>{auto.vin}</option>
                        )
                        })}
                    </select>
                    </div>
                    <div className="mb-3">
                    <select onChange={handleFormChange} required name="sales_person" className="form-select" id="sales_person">
                        <option>Choose an employee</option>
                        {employee.map(employees => {
                        return (
                        <option key={employees.id} value={employees.employee_id}>{employees.name}</option>
                        )
                        })}
                    </select>
                    </div>
                    <div className="mb-3">
                    <select onChange={handleFormChange} required name="customer" className="form-select" id="customer">
                        <option>Choose a customer</option>
                        {customers.map(customer => {
                        return (
                        <option key={customer.id} value={customer.id}>{customer.name}</option>
                        )
                        })}
                    </select>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} placeholder="Price" required name="price" type="number" id="price" className="form-control" />
                        <label htmlFor="price">Price</label>
                    </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                    <div className={messageClasses}  id="success-message">
                    <p className="text-center">Congrats on your big sale!</p>
                    <p className="text-center">Take a 5 minute break you deserve it!</p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default SaleRecordForm;
