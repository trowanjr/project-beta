import { useEffect, useState } from "react"
import { useParams } from 'react-router-dom'

function CustomerList() {
    const [customers, setCustomers] = useState([])
    const { id } = useParams()

    const getData = async ()=> {
        const response = await fetch('http://localhost:8090/api/customers/')
        if (response.ok) {
            const data = await response.json()
            setCustomers(data.customers)
        }
    }

    const handleDelete = async (id) => {
        const response = await fetch(`http://localhost:8090/api/customers/${id}`, {
            method: "DELETE",
        })

        if (response.ok) {
            setCustomers(customers.filter((customer) => customer.id !== id))
        }
    }

    useEffect(() => {
        getData()
    }, [])

    return (
        <>
        <div>
            <h1 className="text-center">Customers List</h1>
            <table className = "table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Customer's Name</th>
                        <th>Customer's Phone Number</th>
                        <th>Customer's Address</th>
                    </tr>
                </thead>
                <tbody>
                    {customers.map(customer => {
                        return (
                            <tr key={ customer.id }>
                                <td>{ customer.name }</td>
                                <td>{ customer.phone_number }</td>
                                <td>{ customer.address }</td>
                                <td>
                                    <button onClick={() => handleDelete(customer.id)}>Delete</button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
        </>
    )
}

export default CustomerList;
