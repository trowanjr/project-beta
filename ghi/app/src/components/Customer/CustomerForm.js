import React, { useState, useEffect } from 'react'

function CustomerForm() {
    const [formData, setFormData] = useState({
        name: '',
        address: '',
        phone_number: ''
    })

    const handleFormChange = e => {
        setFormData({
            ...formData,
            [e.target.name]:e.target.value
        })
    }

    const handleSubmit = async event => {
        event.preventDefault()
        const url = 'http://localhost:8090/api/customers/'
        const fetchConfig = {
            method:"post",
            body: JSON.stringify(formData),
            headers: {"Content-Type": "application/json"}
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            const newResponse = await response.json()

            setFormData({
                name:'',
                address:'',
                phone_number:''
            })
            setHasSignedUp(true)
        } else {
            setHasFailed(true)
        }
    }

    const [hasSignedUp, setHasSignedUp] = useState(false)
    const formClasses = (!hasSignedUp) ? '' : 'd-none';
    const messageClasses = (!hasSignedUp) ? 'alert alert-success d-none mb-0' : 'alert alert-success mb-0';

    const [hasFailed, setHasFailed] = useState(false)
    const formClass = (!hasFailed) ? '' : 'd-none';
    const messageClass=(!hasFailed) ? 'alert alert-warning d-none mb-0' : 'alert alert-danger mb-0';

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1 className="text-center">Add a potential customer</h1>
                    <form className={formClasses - formClass } onSubmit={handleSubmit} id="create-customer-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} value={ formData.name } placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                        <label htmlFor="Name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} value={ formData.address } placeholder="Address" required type="text" name="address" id="address" className="form-control"/>
                        <label htmlFor="Address">Address</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} value={ formData.phone_number } placeholder="Phone Number" required type="text" name="phone_number" id="phone_number" className="form-control"/>
                        <label htmlFor="Phone">Phone Number</label>
                    </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                    <div className={messageClasses}  id="success-message">
                        <p className="text-center">The customer has been added successfully!</p>
                        <p className="text-center">Inform the customer we will be in contact soon and have a great day!</p>
                    </div>
                    <div className={messageClass} id="warning-message">
                    <p className="text-center">Couldn't Create A Customer!</p>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default CustomerForm;
