import { NavLink } from 'react-router-dom';
import './index.css'

function MainPage() {
  return (
    <>
    <div>
      <img className="w-100 card-img opacity-75" src="https://i.ebayimg.com/images/g/A9AAAOSwACRasIrD/s-l1600.jpg" alt="of interstate" />
      <div className="px-4 py-5 mt-5 mx-5 text-center card-img-overlay">
      <div className="card text-center">
        <div className="card-body">
          <h1 className="display-5 fw-bold">CarCar</h1>
          <div className="col-lg-6 mx-auto">
            <p className="lead mb-4">
              The premiere solution for automobile dealership management!
            </p>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="card w-25 mt-5 mx-3 col">
          <div className="card-body">
            <h5 className="card-title fw-bold">Automobile Inventory</h5>
            <p className="card-text">View our current automobile inventory</p>
            <img className="card-img-top" src="/jays inventory .png" alt="" />
            <NavLink className="btn btn-primary mt-2" to="vehicles/">Inventory</NavLink>
          </div>
        </div>
        <div className="card w-25 mt-5 mx-3 col">
          <div className="card-body">
            <h5 className="card-title fw-bold">Lead Sales Manager</h5>
            <p className="card-text">View our top sales member's accomplishments for the month!!!</p>
            <img className="card-img-top1" src="1652213756873.jpg" alt="of car sales person" />
            <NavLink className="btn btn-primary mt-2" to="sales/members/">Sales Center</NavLink>
          </div>
        </div>
        <div className="card w-25 mt-5 mx-3 col">
          <div className="card-body">
            <h5 className="card-title fw-bold">Lead Service Technician</h5>
            <p className="card-text">View and create new employees</p>
            <img className="card-img-top" src="https://i.ytimg.com/vi/esL3T8Hm6Lo/hqdefault.jpg?sqp=-oaymwEcCNACELwBSFXyq4qpAw4IARUAAIhCGAFwAcABBg==&rs=AOn4CLDoDjkM6ihSt51v15TOkkAS3xCu4A" alt="of car being serviced" />
            <NavLink className="btn btn-primary mt-2" to="technicianform">Service Center</NavLink>
          </div>
        </div>
        <div className="card w-25 mt-5 mx-3 col">
          <div className="card-body">
            <h5 className="card-title fw-bold">VIP Customer</h5>
            <p className="card-text">View current service schedule, service history, or manage appointments.</p>
            <img className="card-img-top1" src="\IMG_1002.jpg" alt="of car being serviced" />
            <NavLink className="btn btn-primary mt-2" to="appointmentlist">Appointments</NavLink>
          </div>
        </div>
        </div>
      </div>
    </div>
    </>
  );
}

export default MainPage;
