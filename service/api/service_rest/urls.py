from django.urls import path
from .views import (
    appt_list,
    tech_list,
    appt_detail,
    tech_detail
)


urlpatterns = [
    path('appt/', appt_list, name='appt_list'),
    path('appt/<int:id>/', appt_detail, name='appt_detail'),
    path('technician/', tech_list, name='tech_list'),
    path('technician/<int:id>/', tech_detail, name="tech_detail"),
]
