import json
from django.http import JsonResponse
from .models import Technician, Appointment
from .encoders import TechEncoder, ApptEncoder, ApptDetailEncoder
from django.views.decorators.http import require_http_methods


@require_http_methods(["GET", "POST"])
def tech_list(request):
    if request.method == "GET":
        techs = Technician.objects.all()
        return JsonResponse(
            {'techs': techs},
            encoder=TechEncoder,
        )
    else:
        content = json.loads(request.body)
        tech = Technician.objects.create(**content)
        return JsonResponse(
            tech,
            encoder=TechEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE",])
def tech_detail(request, id):
    if request.method == "GET":
        tech = Technician.objects.get(id=id)
        return JsonResponse(
            tech,
            encoder=TechEncoder,
            safe=False,
        )
    else:
        request.method == "DELETE"
        count, _ = Technician.objects.filter(id=id).delete()
        return JsonResponse({'deleted': count > 0})


@require_http_methods(["GET", "POST"])
def appt_list(request):
    if request.method == "GET":
        appts = Appointment.objects.all()
        return JsonResponse(
            {'appts': appts},
            encoder=ApptEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            technician_id = content["technician"]
            tech = Technician.objects.get(employee_num=technician_id)
            content['technician'] = tech
        except Technician.DoesNotExist:
            return JsonResponse(
                {'message': "Technician does not exist"},
                status=400,
            )
        appt = Appointment.objects.create(**content)
        return JsonResponse(
            appt,
            encoder=ApptDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def appt_detail(request, id):
    if request.method == "GET":
        appt = Appointment.objects.get(id=id)
        return JsonResponse(
            appt,
            encoder=ApptDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Appointment.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Appointment.objects.filter(id=id).update(**content)
        return JsonResponse(
            Appointment.objects.get(id=id),
            encoder=ApptDetailEncoder,
            safe=False,
        )
