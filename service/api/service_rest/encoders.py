from .models import Appointment, Technician, AutomobileVO
from common.json import ModelEncoder


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ['vin' "href"]


class TechEncoder(ModelEncoder):
    model = Technician
    properties = ['name', 'employee_num', 'id']


class ApptEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "vin",
        'cust_name',
        'date',
        'time',
        'technician',
        'reason',
        'id',
    ]
    encoders = {
        'technician': TechEncoder(),
    }


class ApptDetailEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "vin",
        'cust_name',
        'date',
        'time',
        'technician',
        'reason',
        'status',
        'id',
    ]
    encoders = {
        'technician': TechEncoder(),
    }
