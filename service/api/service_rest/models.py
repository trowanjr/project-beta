from django.db import models


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField()
    

class Technician(models.Model):
    name = models.CharField(max_length=200)
    employee_num = models.CharField(max_length=9)

    def __str__(self):
        return self.name


class Appointment(models.Model):
    vin = models.CharField(
        max_length=17,
        blank=True,
        null=True,
    )
    cust_name = models.CharField(max_length=200)
    date = models.CharField(max_length=10)
    time = models.CharField(max_length=7)
    technician = models.ForeignKey(
        Technician,
        on_delete=models.CASCADE,
    )
    reason = models.CharField(max_length=200)
    status = models.BooleanField(default=False, null=True)

    def __str__(self):
        return f'{self.cust_name} {self.date} {self.time}'