# CarCar

Team:

* Zach - Sales Lead
* Rowan - Service Lead

## Design

## Service microservice

I was tasked with working with the Service microservice that includes Service Appointments and History, Technicians, and forms to create both Technicians and Appointments. For my Appointment List I was tasked with checking the Inventory for vin numbers and comparing them to vin numbers for appointments in order to see who was a vip client and who wasn't. I also needed to create a way to cancel appointments and show completed services. In my AppointmentForm I was tasked with creating an appointment that needed that needed to be linked to a specific technician. This selection was based of the employee number. The Service History needed to filtered by the vin number that was found on the Appointment list.

Explain your models and integration with the inventory
microservice, here.

## Sales microservice

My models are linked with the sales microservice in 4 categories. We have a sales member who is linked to a customer, sales record, and automobiles. our automobile is a value object from the inventory microservice to link a VIN number to all of our sales records. This is also used to be able to filter a car that has already been sold, the VIN is the id in this case. a customer can be created to then be linked to our sales team, sales records, and automobileVO. the same can be said for all four of these being linked together.


Our views show our models in several different ways. On the employees side we can see a list of employees or specific details of an employee. An employee will have a name and id associated with them to be able to filter sales by.

The customers view will show either a list of all customers or specific details of a customer. the customer will have their information stored in the database to contact them for future sales and repairs.

We have a sales view that will show a list of all the sales that have been made by our sales team. We can also filter these sales by who made them. sales records will show all of our microservices being linked with the employee who made the sale, the customer who purchased the sale, the vehicle that was sold and its' VIN number associated with the vehicle.

Our last two views are just for specific vehicles in the inventory list, these are sold and unsold cars left in the inventory list. Also we have our final view which will show the sales records for a specific employee, we do this to add a little competitive spirit to the specific month. Our monthly winner receives an all paid vacation to a special coding camp designed by Jerid P. Woods. He is the leading software engineer for the Anaconda ++ program that has taken the nation by surprise.


We have a poll set up to be able to receive data from our Inventory Microservice and update our data.


We also have some encoders set up to translate our data into a string so our end point user can easily read this data and its also machine friendly.


On our front end in order to create a sales record we need to already have our customer created, employee created, and a vehicle to choose from that hasn't already been sold.
every microservice is essentially linked at the end with a vin number being the link to all of them.
