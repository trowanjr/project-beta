import json
from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import SalesPerson, Customer, SalesRecord, AutomobileVO
from .encoders import SalesPersonEncoder, SalesRecordEncoder, CustomerEncoder, AutomobileVOEncoder


@require_http_methods(["GET", "POST"])
def api_sales_person_list(request):
    if request.method == "GET":
        try:
            sales_person = SalesPerson.objects.all()
            return JsonResponse(
                {"sales_person": sales_person},
                encoder=SalesPersonEncoder
            )
        except SalesPerson.DoesNotExist:
            response = JsonResponse(
                {"message": "Sales person does not exist"},
                status = 404
            )
            return response
    else:
        try:
            content = json.loads(request.body)
            sales_person = SalesPerson.objects.create(**content)
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Can't create a sales person, you think I'm a god!?"},
                status = 404,
            )
            return response


@require_http_methods(["GET", "DELETE", "PUT"])
def api_sales_person_details(request, id):
    if request.method == "GET":
        try:
            sales_person = SalesPerson.objects.get(employee_id=id)
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except SalesPerson.DoesNotExist:
            response = JsonResponse(
                {"message": "Does not exist"},
                status = 404
            )
            return response
    elif request.method == "DELETE":
        try:
            sales_person = SalesPerson.objects.get(employee_id=id)
            sales_person.delete()
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except SalesPerson.DoesNotExist:
            return JsonResponse({"message": "Sales person no longer works here"})
    else:
        try:
            content = json.loads(request.body)
            sales_person = SalesPerson.objects.get(employee_id=id)
            props = ["name", "employee_id"]
            for prop in props:
                if prop in content:
                    setattr(sales_person, prop, content[prop])
            sales_person.save()
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except SalesPerson.DoesNotExist:
            response = JsonResponse(
                {"message": "Sales person no longer works here"},
                status = 404,
            )
            return response


@require_http_methods(["GET", "POST"])
def api_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_customer_details(request, id):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=id)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            response = JsonResponse(
                {"message": "Customer not found"},
                status = 404,
            )
            return response
    elif request.method == "DELETE":
        try:
            customer = Customer.objects.get(id=id)
            customer.delete()
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Customer not found"})
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.get(id=id)
            props = ["name", "address", "phone_number"]
            for prop in props:
                if prop in content:
                    setattr(customer, prop, content[prop])
                customer.save()
                return JsonResponse(
                    customer,
                    encoder=CustomerEncoder,
                    safe=False,
                )
        except Customer.DoesNotExist:
            response = JsonResponse(
                {"message": "Customer not found"},
                status=404,
            )
            return response


@require_http_methods(["GET", "POST"])
def api_sales_records(request):
    if request.method == "GET":
        sales_record = SalesRecord.objects.all()
        return JsonResponse(
            {"sales_record": sales_record},
            encoder=SalesRecordEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=vin)
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
                return JsonResponse(
                    {"message": "Automobile not found"},
                    status=400)
        try:
            employee_id = content['sales_person']
            sales_person = SalesPerson.objects.get(employee_id=employee_id)
            content['sales_person'] = sales_person
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": " Sales member not found"},
                status=400)
        try:
            customer_id = content['customer']
            customer = Customer.objects.get(id=customer_id)
            content['customer'] = customer
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Customer not found"},
                status=400)
        sales_record = SalesRecord.objects.create(**content)
        return JsonResponse(
            sales_record,
            encoder=SalesRecordEncoder,
            safe=False
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_sales_record_details(request, id):
    if request.method == "GET":
        try:
            sales_record = SalesRecord.objects.get(id=id)
            return JsonResponse(
                sales_record,
                encoder=SalesRecordEncoder,
                safe=False,
            )
        except SalesRecord.DoesNotExist:
            response = JsonResponse(
                {"message": "Record not found"},
                status=404,
            )
            return response
    elif request.method == "DELETE":
        try:
            sales_record = SalesRecord.objects.filter(id=id)
            sales_record.delete()
            return JsonResponse(
                sales_record,
                encoder=SalesRecordEncoder,
                safe=False,
            )
        except SalesRecord.DoesNotExist:
            response = JsonResponse(
                {"message": "Record not found"},
                status_code = 404,
            )
    else:
        try:
            content = json.loads(request.body)
            sales_record = SalesRecord.objects.filter(id=id)
            sales_record.sales_person = content["sales_person"]
            sales_record.customer = content["customer"]
            sales_record.save()
            return JsonResponse(
                sales_record,
                encoder=SalesRecordEncoder,
                safe=False
            )
        except SalesRecord.DoesNotExist:
            response = JsonResponse(
                {"message": "Could not update the sales record"},
                status_code = 404
            )


@require_http_methods(["GET"])
def api_sales_person_records(request, employee_id=None):
    if request.method == "GET":
        if employee_id is not None:
            employee = SalesPerson.objects.filter()
        else:
            employee = SalesPerson.objects.all()
        return JsonResponse(
            {"employee": employee},
            encoder=SalesRecordEncoder,
        )


@require_http_methods(["GET"])
def api_list_automobiles(request):
    autos = AutomobileVO.objects.all()
    return JsonResponse(
        {"autos": autos},
        encoder = AutomobileVOEncoder,
        safe=False
    )
