from django.db import models


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=200, unique=True, null=True)


class SalesPerson(models.Model):
    name = models.CharField(max_length=100)
    employee_id = models.PositiveSmallIntegerField(unique=True, null=False)

    def __str__(self):
        return self.name


class Customer(models.Model):
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=150)
    phone_number = models.CharField(max_length=10)

    def __str__(self):
        return self.name


class SalesRecord(models.Model):
    automobile = models.ForeignKey(AutomobileVO, related_name="sales_person", on_delete=models.PROTECT)
    sales_person = models.ForeignKey(SalesPerson, related_name="customer", on_delete=models.PROTECT)
    customer = models.ForeignKey(Customer, related_name="automobile", on_delete=models.PROTECT)
    price = models.PositiveIntegerField(null=False)
